## Quick start
1. clone or fork the project
2. branch off from master
3. run ```yarn install```
4. run ```yarn run dev```

### Challenge
Create a weather app that displays a 7 day report based on the city entered.

Modify the following URL to send a request to the url to get the lat/lng from the api 
- https://api.opencagedata.com/geocode/v1/json?q={CITYNAME}&key=37578f9203e745549279acf2f909cfbc

With the resulting lat/lng modify this URL to get the weather report :
- https://api.openweathermap.org/data/2.5/onecall?lat={latitude}&lon={longitude}&exclude=hourly,minutely&units=metric&appid=80d641a4fc656571a4c892f31c2b86be

This challenge with involve taking the city name from an input. Making a api call to the first url to get the latitude and longitude from the data structure. With that call the second api with the latitude and longitude to call the second api. Using the data from the second api create an application to show the daily temperature, humidity percent and feels_like.

If you get stuck reference the branch 'completed' but try to do as much as you can yourself.

### After
Feel free to modify the app and customize/style it to your liking.